#include "board.h"
#include "serie.h"
#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"
#define NUARTS		2
#define BUFFERSIZE	128

static xQueueHandle colarx[NUARTS];
static xQueueHandle colatx[NUARTS];
static unsigned char inicializado[NUARTS] = {0,0};
static unsigned char revisaInic = 0;

int putcharUart(int uart, char dato)
{
	if(!inicializado[uart&0x01]) return -1;
	xQueueSend(colatx[uart&0x01],&dato,portMAX_DELAY);
	return (int)dato;
}

int getcharUart(int uart)
{
	char dato;
	if(!inicializado[uart&0x01]) return -1;
	xQueueReceive(colarx[uart&0x01],&dato,portMAX_DELAY);
	return (int)dato;
}

LPC_USART_T* getPunteroUart(int nuart)
{
	LPC_USART_T* p;
	switch(nuart)
	{
		case __UART_0:  p = LPC_UART0; break;
		case __UART_1:  p = LPC_UART1; break;
		default: p = NULL;
	}
	return p;
}

void setClkUart(int nuart)
{
	switch(nuart)
	{
		case __UART_0:
						 LPC_SYSCTL->PCONP  	|= (1<<3);
		  	 	 	 	 LPC_SYSCTL->PCLKSEL[0]	|=  (1<<6);
		  	 	 	 	 LPC_SYSCTL->PCLKSEL[0]	&= ~(1<<7);
						break;
		case __UART_1:
						LPC_SYSCTL->PCONP 		|= 1<<4;
						LPC_SYSCTL->PCLKSEL[0] 	|=  (1<<8);
						LPC_SYSCTL->PCLKSEL[0]	&= ~(1<<9);
						break;
	}
}

void setPinesUart(int nuart)
{
	switch(nuart)
	{
		case __UART_0:

						LPC_IOCON->PINSEL[0] |=  (1<<5);
						LPC_IOCON->PINSEL[0] &= ~(1<<4);
						LPC_IOCON->PINSEL[0] |=  (1<<7);
						LPC_IOCON->PINSEL[0] &= ~(1<<6);
						break;
		case __UART_1:
						LPC_IOCON->PINSEL[0] |=  (1<<31);
						LPC_IOCON->PINSEL[0] &= ~(1<<30);
						LPC_IOCON->PINSEL[1] |=  (1<<1);
						LPC_IOCON->PINSEL[1] &= ~(1<<0);
						break;
	}
}

//Creo una tarea que cada 10ms se fija si hay que
//transmitir.
void EnviarSerieUart(void *parametros)
{
	int i;
	unsigned char dato;
	LPC_USART_T *p;

	for(;;)
	{
		vTaskDelay(10/portTICK_RATE_MS);
		for(i=0; i<NUARTS; i++)
		{
			if(!inicializado[i]) continue;
			p = getPunteroUart(i);
			if(p->LSR&LSR_TEMT)
			{
				if(uxQueueMessagesWaiting(colatx[i])>0)
				{
					xQueueReceive(colatx[i],&dato,0);
					p->THR = dato;
				}
			}
		}
	}
}


extern void InicializarUart(int nuart, int baudrate)
{
	  uint32_t Fdiv;
	  LPC_USART_T *p;
	  p = getPunteroUart(nuart);
	  setClkUart(nuart);
	  setPinesUart(nuart);

	  p->LCR = 0x83;								/* 8 bits, no Parity, 1 Stop bit */
	  Fdiv = ( SystemCoreClock / 16 ) / baudrate ;	/*baud rate */
	  p->DLM = Fdiv / 256;
	  p->DLL = Fdiv % 256;
	  p->LCR = 0x03;								/* DLAB = 0 */
	  p->FCR = 0x07;								/* Enable and reset TX and RX FIFO. */
	  p->IER = IER_RBR | IER_THRE | IER_RLS;		/* Enable UART3 interrupt */

	  colarx[nuart&1] = xQueueCreate(BUFFERSIZE,sizeof(unsigned char));
	  colatx[nuart&1] = xQueueCreate(BUFFERSIZE,sizeof(unsigned char));
	  inicializado[nuart&1]=1;
	  switch(nuart)
	  {
	  	  case __UART_0:
	  		  	  	  	  NVIC_EnableIRQ(UART0_IRQn);
	  		  	  	  	  NVIC_SetPriority(UART0_IRQn,8);
	  		  	  	  	  break;
	   	  case __UART_1:
	  	  	  	  	  	  NVIC_EnableIRQ(UART1_IRQn);
	  	  	  	  	  	  NVIC_SetPriority(UART1_IRQn,8);
	  	  	  	  	  	  break;
	  }

	  if(!revisaInic)
	  {
		  revisaInic = 1;
		  //Creo una tarea que se fija si hay datos por enviar.
		  xTaskCreate(	EnviarSerieUart,
		  				( signed portCHAR * ) "Serie",
		  				configMINIMAL_STACK_SIZE,
		  				NULL,
		  				tskIDLE_PRIORITY+1,
		  				NULL );
	  }
}



 void UART_IRQ(int uart)
{
	portBASE_TYPE  HigherPriorityTaskWoken = 0;
	int rIIR;
	int i;
	int ret;
	unsigned char dato;
	LPC_USART_T *p;
	p = getPunteroUart(uart);

	//leo para saber que tipo de
	//interrupción se generó.
	rIIR=(p->IIR)>>1;
	rIIR&=0x07;

	switch(rIIR&0x07)
	{
		case IIR_RLS:
					//Por error, en teoría no entro jamás
					//(no la activo en la inicialización)
					i=LPC_UART0->LSR;
					break;
		case IIR_RDA:
		case IIR_CTI:
					//Tengo que leer la FIFO
					//(saltó por disparo de RX)
					for(i=0;i<14;i++)
					{
						if(!((p->LSR)&LSR_RDR)) break;
						dato = p->RBR;
						xQueueSendFromISR(colarx[uart],&dato,&HigherPriorityTaskWoken);
					}
					break;
		case IIR_THRE:
					//Interrupción por Transmisión
					//FIFO de TX vacía, la intento
					//llenar.
					for(i=0;i<16;i++)
					{
						ret = xQueueReceiveFromISR(colatx[uart],&dato,&HigherPriorityTaskWoken);

						if(ret!=errQUEUE_EMPTY)
						{
							p->THR = dato;
						}
						else
						{
							break;
						}
					}
					break;
	}
	portEND_SWITCHING_ISR(HigherPriorityTaskWoken);
}

 void  UART0_IRQHandler(void)
 {
	 UART_IRQ(__UART_0);
 }

 void  UART1_IRQHandler(void)
 {
	 UART_IRQ(__UART_1);
 }
