#include "../../TP3/inc/main.h"

#include "../../TP3/inc/FreeRTOSConfig.h"
#include "board.h"

#include "FreeRTOS.h"
#include "task.h"
#include "string.h"

#include "serie.h"
#include "semphr.h"
#include <stdbool.h>

#include <NXP/crp.h>//esto lo agregue, no se por que todos los otros programas no necesitan agregar esto explicitamente y este si, pero  por ahora hacer esto funciona.
__CRP const unsigned int CRP_WORD = CRP_NO_CRP ;

//Defines de la configuracion serie
#define BAUDRATE 9600

//Defines de mensajes
#define MENSAJE_Inicio_Trama					0xAA
#define MENSAJE_Final_Trama						0x55
#define MENSAJE_Master_Inicial					strcat(strcat(MENSAJE_Inicio_Trama, "01"), MENSAJE_Final_Trama)
#define MENSAJE_Master_ArranqueMotorV01			strcat(strcat(strcat(MENSAJE_Inicio_Trama, "05"), "01"), MENSAJE_Final_Trama)
#define MENSAJE_Master_ArranqueMotorV05			strcat(strcat(strcat(MENSAJE_Inicio_Trama, "05"), "05"), MENSAJE_Final_Trama)
#define MENSAJE_Master_ArranqueMotorV10			strcat(strcat(strcat(MENSAJE_Inicio_Trama, "05"), "10"), MENSAJE_Final_Trama)
#define MENSAJE_Master_ArranqueMotorV20			strcat(strcat(strcat(MENSAJE_Inicio_Trama, "05"), "15"), MENSAJE_Final_Trama)
#define MENSAJE_Master_ParadaMotor				strcat(strcat(MENSAJE_Inicio_Trama, "80"), MENSAJE_Final_Trama)
#define MENSAJE_Slave_OK						strcat(strcat(MENSAJE_Inicio_Trama, "FF"), MENSAJE_Final_Trama)
#define MENSAJE_Slave_NOK						strcat(strcat(MENSAJE_Inicio_Trama, "00"), MENSAJE_Final_Trama)
#define MENSAJE_Slave_ErrorSinEnergia			strcat(strcat(MENSAJE_Inicio_Trama, "30"), MENSAJE_Final_Trama)
#define MENSAJE_Slave_MotorFuncionando			strcat(strcat(MENSAJE_Inicio_Trama, "40"), MENSAJE_Final_Trama)
#define MENSAJE_Slave_VelocidadIncorrecta		strcat(strcat(MENSAJE_Inicio_Trama, "50"), MENSAJE_Final_Trama)
#define MENSAJE_Slave_EnMantenimiento			strcat(strcat(MENSAJE_Inicio_Trama, "60"), MENSAJE_Final_Trama)

#define STX				0
#define ETX				3
#define	DATO_1			1
#define	DATO_2			2

#define VEL1			1
#define VEL2			2
#define VEL3			3
#define VEL4			4

#define INICIO			1
#define START			2
#define STOP			3

#define CHA				1
#define CHB				2

#define FLAGNOK			1
#define FLAGOK			2

#define MOTOR			2, 0
#define PULSADOR		1, 29
#define SIN_ENERGIA		2, 13
#define EN_MANTTO 		0, 11

#define ON				1
#define OFF				0

xSemaphoreHandle xBinSemaphoreInit;
xSemaphoreHandle xBinSemaphoreTx;
xSemaphoreHandle xBinSemaphoreRx;

bool reset;

static uint32_t estadoTx = 0;
static uint32_t estadoRx = 0;

static uint32_t msg_flag = 0;
static uint32_t Dato1 = 0;
static uint32_t Dato2 = 0;
static uint32_t CCom = 0;

static void initHardware(void);

void vTaskInit( void * pvParameters);
void vTaskRecibe( void * pvParameters);
void vTaskEnvia( void * pvParameters);
void Reset(void);


static void initHardware(void)
{
    SystemCoreClockUpdate();

    Board_Init();

    //Agrego la inicializacion de la UART del LPC
    InicializarUart (0, BAUDRATE);
	InicializarUart (1, BAUDRATE);

	Chip_IOCON_PinMux(LPC_IOCON, PULSADOR, IOCON_MODE_PULLUP, IOCON_FUNC0); //PULSADOR
	Chip_GPIO_SetPinDIRInput(LPC_GPIO1_BASE, PULSADOR);
	Chip_IOCON_PinMux(LPC_IOCON, SIN_ENERGIA, IOCON_MODE_PULLUP, IOCON_FUNC0); //SIN ENERGIA
	Chip_GPIO_SetPinDIRInput(LPC_GPIO1_BASE, SIN_ENERGIA);
	Chip_IOCON_PinMux(LPC_IOCON, EN_MANTTO, IOCON_MODE_PULLUP, IOCON_FUNC0); //EN MANTENIMIENTO
	Chip_GPIO_SetPinDIRInput(LPC_GPIO1_BASE, EN_MANTTO);
	Chip_IOCON_PinMux(LPC_IOCON, MOTOR, IOCON_MODE_PULLUP, IOCON_FUNC0); //MOTOR
	Chip_GPIO_SetPinDIROutput(LPC_GPIO2_BASE, MOTOR);

    Board_LED_Set(0, false);
}



void vTaskInit( void * pvParameters)
{
	xSemaphoreTake( xBinSemaphoreInit, 0 );
	for(;;)
	{
		Reset();
		Board_UARTPutSTR(MENSAJE_Master_Inicial);
		xSemaphoreGive( xBinSemaphoreRx );
		xSemaphoreTake( xBinSemaphoreInit, portMAX_DELAY );
	}
}


void vTaskRecibe( void * pvParameters)
{
	uint8_t datoLeidoA = 0, datoLeidoB = 0; // se almacena temporalmente el dato leido por puerto serie

	xSemaphoreTake( xBinSemaphoreRx, portMAX_DELAY );
	for(;;)
	{
		//Leo en ambas UART. ¿Por qué?
		//Porque existe la posibilidad de que A tenga conectado al módulo de control y B al motor.
		//O que A tenga conectado el motor y B al módulo de control.
		datoLeidoA = getcharUart(0);
		datoLeidoB = getcharUart(1);

		if(datoLeidoA != 256 && datoLeidoB != 256)
		{
			switch(estadoRx)
			{

			case STX:
			//Aquí leo si el primer dato recibido es STX y en ese caso procedo al próximo dato,
			//o si recibo algo erróneo y en ese caso empiezo la trama otra vez.
				if ( datoLeidoA == 170 || datoLeidoB == 170 ) //STX = 0xAA
				{
					estadoRx = DATO_1;
				}
				else
				{
					msg_flag = FLAGNOK;
					estadoRx = STX;
				}
				break;

			case DATO_1:
				//Aquí leo si el dato recibido corresponde al mensaje de inicio...
				if ( datoLeidoA == 1 || datoLeidoB == 1 ) // INICIO = 0x01
				{
					Dato1 = INICIO;
					estadoRx = ETX;
				}
				//Aquí leo si el primer dato recibido corresponde al mensaje de parada de motor...
				else if ( datoLeidoA == 128 || datoLeidoB == 128 ) // STOP = 0x80
				{
					Dato1 = STOP;
					estadoRx = ETX;
				}
				//Aquí leo si el primer dato recibido corresponde al mensaje de arranque de motor...
				else if ( datoLeidoA == 5 || datoLeidoB == 5 ) // START = 0x05
				{
					Dato1 = START;
					estadoRx = DATO_2;
				}
				else
				{
					msg_flag = FLAGNOK;
					estadoRx = STX;
				}
				break;

			case DATO_2:
				//Dato2ocidad 1
				if ( datoLeidoA == 1 || datoLeidoB == 1 ) // VEL = 1
				{
					Dato2 = VEL1;
					estadoRx = ETX;
				}
				//Dato2ocidad 2
				else if ( datoLeidoA == 5 || datoLeidoB == 5 ) // VEL = 5
				{
					Dato2 = VEL2;
					estadoRx = ETX;
				}
				//Dato2ocidad 3
				else if ( datoLeidoA == 10 || datoLeidoB == 10 ) // VEL = 10
				{
					Dato2 = VEL3;
					estadoRx = ETX;
				}
				//Dato2ocidad 4
				else if ( datoLeidoA == 5 || datoLeidoB == 5 ) // VEL = 20
				{
					Dato2 = VEL4;
					estadoRx = ETX;
				}
				else
				{
					msg_flag = FLAGNOK;
					estadoRx = STX;
				}
				break;

			case ETX:
				if ( datoLeidoA == 85 || datoLeidoB == 85 ) // ETX = 0x55
				{
					if ( datoLeidoA == 85 )
						CCom = CHA;
					else
						CCom = CHB;
					msg_flag = FLAGOK;
					estadoRx = STX;
					xSemaphoreGive( xBinSemaphoreTx );
					xSemaphoreTake( xBinSemaphoreRx, portMAX_DELAY );
				}
				else
				{
					msg_flag = FLAGNOK;
					estadoRx = STX;
				}
				break;

			default:
				msg_flag = FLAGNOK;
				estadoRx = STX;
				break;
			} // end switch
		} // end if

		if(reset)
			xSemaphoreGive( xBinSemaphoreInit );
	} // end for
}


void vTaskEnvia( void *pvParameters)
{
	uint8_t nuart, se_causa, em_causa, estado;
	xSemaphoreTake( xBinSemaphoreTx, portMAX_DELAY );

	if ( CCom == CHA )
		nuart = 0;
	if ( CCom == CHB )
		nuart = 1;

	estado = Chip_GPIO_ReadPortBit(LPC_GPIO, PULSADOR);
	se_causa = Chip_GPIO_ReadPortBit(LPC_GPIO, SIN_ENERGIA);
	em_causa = Chip_GPIO_ReadPortBit(LPC_GPIO, EN_MANTTO);

	if ( se_causa == ON )
	{
		putcharUart(nuart, (int)MENSAJE_Slave_ErrorSinEnergia);
		xSemaphoreGive( xBinSemaphoreRx );
		xSemaphoreTake( xBinSemaphoreTx, portMAX_DELAY );
		vTaskDelay( 1000 / portTICK_RATE_MS);
	}

	if ( em_causa == ON )
	{
		putcharUart(nuart, (int)MENSAJE_Slave_EnMantenimiento);
		xSemaphoreGive( xBinSemaphoreRx );
		xSemaphoreTake( xBinSemaphoreTx, portMAX_DELAY );
		vTaskDelay( 1000 / portTICK_RATE_MS);
	}

	//Analizo mensaje recibido OK o NOK.
	switch ( msg_flag )
	{

		//Si hubo algún error en el mensaje...
		case FLAGNOK:
			//Si la velocidad ingresada es incorrecta...
			if ( Dato2 != VEL1 && Dato2 != VEL2 && Dato2 != VEL3 && Dato2 != VEL4 )
				putcharUart(nuart, (int)MENSAJE_Slave_VelocidadIncorrecta);
			//Caso contrario simplemente avisa que el mensaje recibido fue incorrecto...
			else
				putcharUart(nuart, (int)MENSAJE_Slave_NOK);
			break;

		//Si el mensaje fue recibido OK...
		case FLAGOK:
			//Se recibe mensaje de inicio OK, y se devuelve un OK para cerrar cilco de comunicación entre un equipo y otro.
			if ( Dato1 == INICIO )
				putcharUart(nuart, (int)MENSAJE_Slave_OK);
			break;
	}

	//Analizo estado de pulsador de arranque/parada de motor.
	switch ( estado )
	{
		//PULSADOR en estado OFF...
		case OFF:
			Chip_GPIO_WritePortBit(LPC_GPIO, MOTOR, OFF);
			putcharUart(nuart, (int)MENSAJE_Master_ParadaMotor);
			reset = true;
			break;
		//PULSADOR en estado ON...
		case ON:
			//Si reset = false significa que fue parado recientemente el motor.
			if (reset == false )
			{
				Chip_GPIO_WritePortBit(LPC_GPIO, MOTOR, ON);
				switch ( Dato2 )
				{
					case VEL1:
						putcharUart(nuart, (int)MENSAJE_Master_ArranqueMotorV01);
						break;
					case VEL2:
						putcharUart(nuart, (int)MENSAJE_Master_ArranqueMotorV05);
						break;
					case VEL3:
						putcharUart(nuart, (int)MENSAJE_Master_ArranqueMotorV10);
						break;
					case VEL4:
						putcharUart(nuart, (int)MENSAJE_Master_ArranqueMotorV20);
						break;
				}
			}
			else
				if ( Dato2 == VEL1 || Dato2 == VEL2 || Dato2 == VEL3 || Dato2 == VEL4 )
					putcharUart(nuart, (int)MENSAJE_Slave_MotorFuncionando);
	}
	xSemaphoreGive( xBinSemaphoreRx );
	xSemaphoreTake( xBinSemaphoreTx, portMAX_DELAY );
}


void Reset(void)
{
	estadoTx = 0;
	estadoRx = 0;
	msg_flag = 0;
	Dato2 = 0;
	CCom = 0;
	reset = false;
}

int main(void)
{
	initHardware();

	vSemaphoreCreateBinary( xBinSemaphoreInit );
	vSemaphoreCreateBinary( xBinSemaphoreRx );
	vSemaphoreCreateBinary( xBinSemaphoreTx );

	xTaskCreate(vTaskInit, (const signed char *)"Init", configMINIMAL_STACK_SIZE*2, 0, tskIDLE_PRIORITY+3, 0);
	xTaskCreate(vTaskRecibe, (const signed char *)"Recibe", configMINIMAL_STACK_SIZE*2, 0, tskIDLE_PRIORITY+2, 0);
	xTaskCreate(vTaskEnvia, (const signed char *)"Envia", configMINIMAL_STACK_SIZE*2, 0, tskIDLE_PRIORITY+1, 0);

	vTaskStartScheduler();

	while (1) {
	}
}


